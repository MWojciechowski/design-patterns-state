﻿using System;

namespace State_Design_Pattern.Logic
{
	public class ClosedState : BookingState
	{
		public string ReasonClosed { get; private set; }
		public ClosedState(string reason)
		{
			ReasonClosed = reason;
		}

		public override void Cancel(BookingContext booking)
		{
			booking.View.ShowError("Can not invoke cancel of closed state", "Closed Booking Error");
		}

		public override void DatePassed(BookingContext booking)
		{
			booking.View.ShowError("Can not invoke date passed of closed state", "Closed Booking Error");
		}

		public override void EnterDetails(BookingContext booking, string attendee, int ticketCount)
		{
			booking.View.ShowError("Can not invoke enter details of closed state", "Closed Booking Error");
		}

		public override void EnterState(BookingContext booking)
		{
			booking.ShowState("Closed");
			booking.View.ShowStatusPage(ReasonClosed);
		}
	}
}
